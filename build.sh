#!/bin/bash
curl -L https://github.com/tdewolff/minify/releases/download/v2.10.0/minify_linux_amd64.tar.gz -o minify.tar.gz
tar -xf minify.tar.gz
chmod a+x minify
mkdir public
cp -r source/* public
./minify -r public -o .