exports.handler = async function (event, context) {
  return {
    statusCode: 200,
    headers: { server: "nginx"},
    body: JSON.stringify({ message: "A beauty once in four thousand years.", author: "kikuchanj fans."}),
  };
}