import requests
import io,json
from PIL import Image

def getwh(url):
    r = requests.get(url)
    m = Image.open(io.BytesIO(r.content))
    return m.width,m.height

def setwh():
    f = open('data.json','r')
    l = json.load(f)
    f.close()
    rs = []
    for i,it in enumrate(l):
        w,h = getwh(it.url)
        rs.append({'url':it.url,'title':f'title{i+1}','width':w,'height':h})

    with open('images.json','w') as f:
        f.write(json.dumps(rs,indent=2,ensure_ascii=False))

def iter_title():
    with open('experience.txt','r',encoding='utf-8') as f:
        titles = [x.strip() for x in f.readlines()]
    n = len(titles)
    i = 0
    while True:
        yield titles[i % n]
        i=i+1

def set_title():
    f = open('images1.json','r')
    l = json.load(f)
    f.close()
    rs = []
    itt = iter_title()
    n = len(l)
    for i in range(n):
        l[i]['title'] = next(itt)
    with open('images2.json','w',encoding='utf-8') as f:
        f.write(json.dumps(l,indent=2,ensure_ascii=False))

set_title()