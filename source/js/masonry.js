var MiniMasonry = function(conf) {
    this._sizes             = [];
    this._columns           = [];
    this._container         = null;
    this._containerWidth    = 0;

    this.conf = {
        colWidth: 200,
        gutterX: null,
        gutterY: null,
        gutter: 10,
        columnCount: null, // elements count per row, auto compute if not specified. 
        container: null,
        minify: true,
        surroundingGutter: true,
    };

    this.init(conf);

    return this;
}

MiniMasonry.prototype.init = function(conf) {
    for (let k in conf) {
        if (conf[k] != undefined) {
            this.conf[k] = conf[k];
        }
    }
    if (this.conf.gutterX == null || this.conf.gutterY == null) {
        this.conf.gutterX = this.conf.gutterY = this.conf.gutter;
    }

    this._container = typeof this.conf.container == 'object' && this.conf.container.nodeName ?
        this.conf.container :
        document.querySelector(this.conf.container);

    if (!this._container) {
        throw new Error('Container not found or missing');
    }
    this._containerWidth = this._container.getBoundingClientRect().width;
    this.layout();
};

//calculate how many elements per row
MiniMasonry.prototype.calcCount = function(){
    if (this.conf.surroundingGutter) {
        return Math.floor((this._containerWidth - this.conf.gutterX) / (this.conf.colWidth + this.conf.gutterX));
    }

    return Math.floor((this._containerWidth + this.conf.gutterX) / (this.conf.colWidth + this.conf.gutterX));

}
MiniMasonry.prototype.getCount = function() {
    if(this.conf.count){
        return this.conf.count;
    }else{
        this.conf.count = this.calcCount();
        return this.conf.count;
    }
}

MiniMasonry.prototype.layout =  function() {
    if (!this._container) {
        throw new Error('Container not found or missing');
    }
    let c = this.getCount();
    
    for (let i = 0; i < c; i++) {
        this._columns[i] = 0;
    }

    //Saving children real heights
    let children = this._container.children;
    for (let k = 0;k< children.length; k++) {
        // Set colWidth before retrieving element height if content is proportional
        children[k].style.width = this.conf.colWidth + 'px';
        this._sizes[k] = children[k].clientHeight;
    }
    let ew = this.conf.colWidth + this.conf.gutterX;

    let a = (this._containerWidth - (c * ew) - this.conf.gutterX) / 2;
    let b = (this._containerWidth - (c * ew) + this.conf.gutterX) / 2;
    let marginLeft = this.conf.surroundingGutter ? a : b; 
    //Computing position of children
    for (let index = 0;index < children.length; index++) {
        let nextColumn = this.conf.minify ? this.getShortest() : this.getNextColumn(index);

        let x = marginLeft + (this.conf.surroundingGutter ? this.conf.gutterX : 0) + ((this.conf.colWidth + this.conf.gutterX) * (nextColumn));
        let y = this._columns[nextColumn];

        children[index].style.transform = 'translate3d(' + Math.round(x) + 'px,' + Math.round(y) + 'px,0)';
        this._columns[nextColumn] += this._sizes[index] + this.conf.gutterY;
        //this._columns[nextColumn] += this._sizes[index] + (count > 1 ? this.conf.gutterY : this.conf.ultimateGutter);//margin-bottom
    }

    this._container.style.height = (this._columns[this.getLongest()] + this.conf.gutterY) + 'px';
};

MiniMasonry.prototype.getNextColumn = function(index) {
    return index % this._columns.length;
};

MiniMasonry.prototype.getShortest = function() {
    let shortest = 0;
    let c = this.getCount();
    for (let i = 0; i < c; i++) {
        if (this._columns[i] < this._columns[shortest]) {
            shortest = i;
        }
    }

    return shortest;
};

MiniMasonry.prototype.getLongest = function() {
    let longest = 0;
    let c = this.getCount();
    for (let i = 0; i < c; i++) {
        if (this._columns[i] > this._columns[longest]) {
            longest = i;
        }
    }

    return longest;
};
export default MiniMasonry;
